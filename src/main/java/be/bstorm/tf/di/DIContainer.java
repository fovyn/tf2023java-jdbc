package be.bstorm.tf.di;

import be.bstorm.tf.annotations.Repository;
import be.bstorm.tf.annotations.Service;
import be.bstorm.tf.configs.ApplicationConfig;
import be.bstorm.tf.utils.YamlReader;
import lombok.SneakyThrows;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.util.*;

public class DIContainer {
    private final ApplicationConfig config;
    private final Map<Class<?>, Object> services = new HashMap<>();

    @SneakyThrows
    public DIContainer(String configPath) {
        this.config = YamlReader.read(ApplicationConfig.class, configPath);
        initConfigServices(this.config);
        initRepositories();
        initServices();
    }

    @SneakyThrows
    private void initServices() {
        Reflections reflections = new Reflections("be.bstorm.tf");
        Set<Class<?>> services = reflections.getTypesAnnotatedWith(Service.class);

        for (Class<?> sClass : services) {
            this.addServices(sClass);
        }
    }

    @SneakyThrows
    private void initRepositories() {
        Reflections reflections = new Reflections("be.bstorm.tf");
        Set<Class<?>> repositories = reflections.getTypesAnnotatedWith(Repository.class);

        for (Class<?> rClass : repositories) {
            this.addServices(rClass);
        }
    }

    @SneakyThrows
    private void initConfigServices(ApplicationConfig config) {
        addServices(config.getClass(), config);
        Field[] fields = config.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            addServices(field.getType(), field.get(config)); //addServices(DatasouceConfig.class, config.datasource)
        }
    }

    public void addServices(Class<?> serviceType, Object service) {
        this.services.put(serviceType, service);
    }

    @SneakyThrows
    public <T> void addServices(Class<T> serviceType) {
        Constructor<?>[] constructors = serviceType.getConstructors();

        for (Constructor<?> constructor : constructors) {
            if (constructor.getParameterCount() > 0) {
                Object[] parameters = resolveParameters(constructor);
                Object instance = constructor.newInstance(parameters);
                this.services.put(serviceType, instance);
            } else {
                this.services.put(serviceType, constructor.newInstance());
            }
        }
    }

    public <T> T get(Class<T> tClass) {
        return (T) services.get(tClass);
    }

    @SneakyThrows
    private Object[] resolveParameters(Constructor<?> constructor) {
        Parameter[] parameters = constructor.getParameters();
        List<Object> values = new ArrayList<>();

        for (Parameter parameter : parameters) {
            Class<?> pClass = parameter.getType();
            if (services.containsKey(pClass)) {
                values.add(this.get(pClass));
            } else {
                addServices(pClass);
                values.add(this.get(pClass));
            }
        }

        return values.toArray();
    }
}
