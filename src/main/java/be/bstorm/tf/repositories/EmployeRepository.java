package be.bstorm.tf.repositories;

import be.bstorm.tf.annotations.Repository;
import be.bstorm.tf.configs.DataSourceConfig;
import be.bstorm.tf.models.Employe;
import lombok.SneakyThrows;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

@Repository
public class EmployeRepository extends BaseRepository<Employe, Long> implements IEmployeRepository {

    public EmployeRepository(DataSourceConfig config) {
        super(config);
    }

    @Override
    @SneakyThrows
    public Employe insert(Employe employe) {
        String sql = "INSERT INTO employe(firstname, lastname, birthDate) VALUES (?, ?, NOW())";

        try (
                Connection connection = openConnection();
                PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, employe.getFirstname());
            statement.setString(2, employe.getLastname());

            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                employe.setId(rs.getLong(1));
            }
            return employe;
        }
    }

    @Override
    public Employe update(Long id, Employe employe) {
        return null;
    }


    @Override
    public void delete(Long id) {

    }

    @Override
    @SneakyThrows
    public Employe fromResultSet(ResultSet rs) {
        return Employe.builder()
                .id(rs.getLong("employe_id"))
                .firstname(rs.getString("firstname"))
                .lastname(rs.getString("lastname"))
                .birthDate(rs.getDate("birthDate"))
                .build();
    }

    @Override
    public Employe findOneByLastname(String lastname) {
        return this.findOne(Map.of("lastname", lastname));
    }

    @Override
    public Employe findOneByFirstname(String firstname) {
        return this.findOne(Map.of("firstname", firstname));
    }

    @Override
    public Employe findOneByFirstnameAndLastname(String firstname, String lastname) {
        return this.findOne(Map.of("firstname", firstname, "lastname", lastname));
    }
}
