package be.bstorm.tf.repositories;

import be.bstorm.tf.models.Employe;

public interface IEmployeRepository extends Repository<Employe, Long> {
    Employe findOneByLastname(String lastname);

    Employe findOneByFirstname(String firstname);

    Employe findOneByFirstnameAndLastname(String firstname, String lastname);
}
