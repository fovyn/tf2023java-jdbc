package be.bstorm.tf.repositories;

import be.bstorm.tf.annotations.Repository;
import be.bstorm.tf.configs.DataSourceConfig;
import be.bstorm.tf.models.Department;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class DepartmentRepository extends BaseRepository<Department, Integer> {
    public DepartmentRepository(DataSourceConfig config) {
        super(config);
    }

    @Override
    public Department insert(Department department) {
        return null;
    }

    @Override
    public Department update(Integer id, Department department) {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Department fromResultSet(ResultSet rs) throws SQLException {
        return Department.builder()
                .id(rs.getInt("department_id"))
                .name(rs.getString("name"))
                .build();
    }
}
