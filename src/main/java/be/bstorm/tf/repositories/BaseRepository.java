package be.bstorm.tf.repositories;

import be.bstorm.tf.annotations.jdbc.Entity;
import be.bstorm.tf.annotations.jdbc.Id;
import be.bstorm.tf.configs.DataSourceConfig;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.*;
import java.text.MessageFormat;
import java.util.*;

public abstract class BaseRepository<TEntity, TKey> implements Repository<TEntity, TKey> {
    private final DataSourceConfig config;

    public BaseRepository(DataSourceConfig config) {
        this.config = config;
    }

    @SneakyThrows
    public Connection openConnection() {
        String url = this.config.getUrl();
        String username = this.config.getUsername();
        String password = this.config.getPassword();

        return DriverManager.getConnection(url, username, password);
    }

    private Class<TEntity> getTEntityClass() {
        Type[] typeArguments = ((ParameterizedType) (getClass().getGenericSuperclass())).getActualTypeArguments();
        return (Class<TEntity>) typeArguments[0];
    }

    private Field getFieldId() {
        Class<TEntity> tEntityClass = getTEntityClass();

        return (Field) Arrays.stream(tEntityClass.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(Id.class))
                .toArray()[0];
    }

    private String getEntityName() {
        return getTEntityClass().getAnnotation(Entity.class).name();
    }

    @Override
    @SneakyThrows
    public List<TEntity> findAll() {

        String entityName = getEntityName();
        String sql = MessageFormat.format("SELECT * FROM {0}", entityName);
        List<TEntity> data = new LinkedList<>();
        try (
                Connection connection = openConnection();
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(sql)
        ) {
            while (rs.next()) {
                data.add(fromResultSet(rs));
            }

            return data;
        }
    }

    @Override
    @SneakyThrows
    public TEntity findOne(TKey id) {
        Field fId = getFieldId();
        String idName = fId.getName();
        return this.findOne(Map.of(idName, id));
//        String entityName = getEntityName();
//        Field fId = getFieldId();
//        String idName = fId.getName();
//
//        String sql = MessageFormat.format("SELECT * FROM {0} WHERE {1} = ?", entityName, idName);
//        try (
//                Connection connection = openConnection();
//                PreparedStatement statement = connection.prepareStatement(sql);
//                ResultSet resultSet = statement.executeQuery()
//        ) {
//            resultSet.next();
//
//            return fromResultSet(resultSet);
//        }
    }

    @SneakyThrows
    protected TEntity findOne(Map<String, Object> values) {
        String entityName = getEntityName();
        StringBuilder builder = new StringBuilder("SELECT * FROM ")
                .append(entityName)
                .append(" WHERE ");

        List<Object> objects = new ArrayList<>();
        List<Map.Entry<String, Object>> entries = values.entrySet().stream().toList();

        for (int i = 0; i < entries.size() - 1; i++) {
            Map.Entry<String, Object> entry = entries.get(i);
            builder
                    .append(entry.getKey())
                    .append(" = ?");
            
            if (i < entries.size()) {
                builder.append(" AND ");
            }
            objects.add(entry.getValue());
        }

        builder
                .append(entries.get(entries.size() - 1).getKey())
                .append(" = ?");

        objects.add(entries.get(entries.size() - 1).getValue());

        try (
                Connection connection = openConnection();
                PreparedStatement statement = connection.prepareStatement(builder.toString())
        ) {
            for (int i = 0; i < objects.size(); i++) {
                statement.setObject(i + 1, objects.get(i));
            }

            ResultSet rs = statement.executeQuery();

            rs.next();

            return fromResultSet(rs);
        }
    }

    @SneakyThrows
    @Override
    public TEntity save(TEntity entity) {
        Field id = getFieldId();
        id.setAccessible(true);
        Object idValue = id.get(entity);

        if (idValue == null) {
            return this.insert(entity);
        }
        return this.update((TKey) idValue, entity);
    }
}
