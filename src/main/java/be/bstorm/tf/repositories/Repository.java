package be.bstorm.tf.repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface Repository<TEntity, TKey> {
    List<TEntity> findAll();
    TEntity findOne(TKey id);
    TEntity save(TEntity entity);
    TEntity insert(TEntity entity);
    TEntity update(TKey id, TEntity entity);
    void delete(TKey id);

    TEntity fromResultSet(ResultSet rs) throws SQLException;
}
