package be.bstorm.tf.configs;

import lombok.Data;

@Data
public class ApplicationConfig {
    private String name;
    private DataSourceConfig datasource;
}
