package be.bstorm.tf.configs;

import lombok.Data;
import lombok.Getter;

@Data
public class DataSourceConfig {
    private String url;
    private String username;
    private String password;
    private String driverClassname;
}
