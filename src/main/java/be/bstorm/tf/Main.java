package be.bstorm.tf;

import be.bstorm.tf.di.DIContainer;
import be.bstorm.tf.models.Employe;
import be.bstorm.tf.repositories.EmployeRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main {

    public static void main(String[] args) {
        DIContainer container = new DIContainer("application.yml");

        EmployeRepository repository = container.get(EmployeRepository.class);
        repository.findAll().forEach(System.out::println);

        Employe employe = repository.findOneByLastname("Ovyn");
        System.out.println(employe);
        employe = repository.findOneByFirstname("Flavian");
        System.out.println(employe);
        employe = repository.findOneByFirstnameAndLastname("Flavian", "Ovyn");
        System.out.println(employe);
    }
}