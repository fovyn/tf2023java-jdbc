package be.bstorm.tf.models;

import be.bstorm.tf.annotations.jdbc.Entity;
import be.bstorm.tf.annotations.jdbc.Id;
import lombok.*;

import java.sql.Date;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "employe")
public class Employe {
    @Id(columnName= "employe_id")
    private Long id;
    private String lastname;
    private String firstname;
    private Date birthDate;

    public LocalDate getBirthDate() {
        return birthDate.toLocalDate();
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = Date.valueOf(birthDate);
    }
}
