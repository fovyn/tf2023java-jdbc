package be.bstorm.tf.models;

import be.bstorm.tf.annotations.jdbc.Entity;
import be.bstorm.tf.annotations.jdbc.Id;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Entity(name = "department")
public class Department {
    @Id(columnName = "department_id")
    private Integer id;
    private String name;
}
