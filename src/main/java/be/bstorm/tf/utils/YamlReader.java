package be.bstorm.tf.utils;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;

public final class YamlReader {

    public static <T> T read(Class<T> targetClass, String path) {
        InputStream stream = targetClass.getClassLoader().getResourceAsStream(path);

        Yaml yaml = new Yaml(new Constructor(targetClass));
        T data = yaml.load(stream);

        return data;
    }
}
