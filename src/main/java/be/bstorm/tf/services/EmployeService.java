package be.bstorm.tf.services;

import be.bstorm.tf.annotations.Service;
import be.bstorm.tf.models.Employe;
import be.bstorm.tf.repositories.EmployeRepository;

import java.util.List;

@Service
public class EmployeService {
    private EmployeRepository repository;

    public EmployeService(EmployeRepository repository) {
        this.repository = repository;
    }

    public List<Employe> findAll() {
        return this.repository.findAll();
    }
}
